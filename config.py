from libqtile import hook, layout
from libqtile.config import Match

from screens import screens  # screen/bar configuration
from keys import keys  # key bindings
from groups import groups  # group configuration
from mouse import mouse  # mouse bindings

import os, subprocess

# the superior layout
layouts = [
    layout.MonadTall(
        border_width=1,
        margin=10,
        single_margin=0,
        single_border_width=0,
        border_focus="#215578",
        change_ratio=0.01,
    ),
    layout.MonadWide(
        border_width=1,
        margin=10,
        single_margin=0,
        single_border_width=0,
        border_focus="#215578",
        change_ratio=0.01,
    ),
]


@hook.subscribe.startup_once
def autostart():
    script = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.run([script])


# config defaults
dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = False
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wl_input_rules = None
wmname = "LG3D"
