from libqtile.config import Group
from libqtile.lazy import lazy
from libqtile import qtile

groups = [Group(f"{i}_{j}", label=j) for i in range(2) for j in "123456789"]


def with_screen(scr):
    return list(filter(lambda x: int(x.name.split("_")[0]) == scr.index, qtile.groups))


@lazy.function
def switch_group(qtile, x):
    scr = qtile.current_screen
    group = qtile.groups_map[with_screen(scr)[x - 1].name]
    group.toscreen()


@lazy.function
def move_window(qtile, x):
    scr = qtile.current_screen
    win = scr.group.current_window
    group = with_screen(scr)[x - 1].name
    win.togroup(group)
