from libqtile.config import Group
from libqtile.config import EzKey as Key
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from libqtile import qtile
from pprint import pformat
from groups import switch_group, move_window

mod = "mod4"  # windows key
terminal = guess_terminal()


def swap_screen(q):
    i = q.current_screen.index  # get current screen's index
    # move current window to other screen
    q.current_window.togroup(q.screens[1 if i == 0 else 0].group.name)
    q.cmd_to_screen(1 if i == 0 else 0)  # focus other screen


def focus_screen(q):
    i = q.current_screen.index  # get current screen's index
    q.cmd_to_screen(1 if i == 0 else 0)  # focus other screen


keys = [
    Key("M-a", lazy.layout.shrink(), desc="Shrink window"),
    Key("M-z", lazy.layout.grow(), desc="Grow window"),
    Key("M-l", lazy.spawn("i3lock-fancy"), desc="Lock screen"),
    Key("M-j", lazy.layout.down(), desc="Move focus down"),
    Key("M-k", lazy.layout.up(), desc="Move focus up"),
    Key("M-S-j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key("M-S-k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key("M-n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key("M-f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),
    Key("M-b", lazy.hide_show_bar(), desc="Toggle bar"),
    Key("M-t", lazy.window.toggle_floating(), desc="Toggle float"),
    Key("M-s", lazy.function(focus_screen), desc="Focus other screen"),
    Key("M-S-s", lazy.function(swap_screen), desc="Move window to other screen"),
    Key("M-<return>", lazy.spawn(terminal), desc="Launch terminal"),
    Key("M-<Tab>", lazy.next_layout(), desc="Toggle between layouts"),
    Key("M-w", lazy.window.kill(), desc="Kill focused window"),
    Key("M-C-r", lazy.reload_config(), desc="Reload the config"),
    Key("M-C-q", lazy.spawn("pkill -9 qtile"), desc="Shutdown Qtile"),
    Key("M-<space>", lazy.spawn("rofi -show drun"), desc="Spawn a command using rofi"),
    Key(
        "<XF86MonBrightnessUp>",
        lazy.spawn("xbacklight +5"),
        desc="Primary backlight up",
    ),
    Key(
        "<XF86MonBrightnessDown>",
        lazy.spawn("xbacklight -5"),
        desc="Primary backlight down",
    ),
    Key(
        "<XF86AudioRaiseVolume>",
        lazy.spawn("pulsemixer --change-volume +5"),
        desc="Raise volume",
    ),
    Key(
        "<XF86AudioLowerVolume>",
        lazy.spawn("pulsemixer --change-volume -5"),
        desc="Lower volume",
    ),
    Key("<XF86AudioMute>", lazy.spawn("pulsemixer --toggle-mute"), desc="Mute"),
]

for i in range(1, 10):
    keys.extend([Key(f"M-{i}", switch_group(i)), Key(f"M-S-{i}", move_window(i))])
