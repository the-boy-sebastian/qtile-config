from libqtile import bar, widget # needed libraries to build the bars
from libqtile.lazy import lazy # needed library for mouse callbacks
# from secrets import lfm_key # get api key from secrets file
# from widget import NowPlaying # lastfm now playing widget
from widget import UPowerBattRemaining

# taken from original config file
widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3)
extension_defaults = widget_defaults.copy()

# now playing mouse callback
np_mouse = {"Button1":
    lazy.widget["nowplaying"].function(lambda w: w.update(w.poll()))}

# now playing config
np_conf = dict(
    width=200, scroll=True, scroll_interval=0.05,
    scroll_delay=0, scroll_clear=True, update_interval=None,
    markup=False, mouse_callbacks=np_mouse)

# create bar for primary screen
primary_bar = bar.Bar([
    widget.GroupBox( # groupbox for groups 1-4
        visible_groups=[f"{0}_{i}" for i in "123456789"],
        highlight_method="line",
        highlight_color=["222", "282828"],
        inactive="#aaa",
        hide_unused=True),
        widget.CurrentLayout(),
        widget.WindowName(), # show window name
        widget.Spacer(length=bar.STRETCH),
        #NowPlaying("lyiriyah", lfm_key, **np_conf), # generate now playing text
        widget.Sep(padding=10, size_percent=65),
        widget.Wttr(location={"Croydon": "croydon"}, format="1"), # get weather
        widget.Volume(fmt="vol {}"),
        widget.Sep(padding=10, size_percent=65),
        widget.Battery(charge_char="^ ", discharge_char="",
            format="{char}{percent:2.0%}",
            unknown_char="= "
        ),
        UPowerBattRemaining(update_interval=300, mouse_callbacks={"Button1": lazy.widget["upowerbattremaining"].function(lambda w: w.update(w.poll()))}),
        widget.Sep(padding=10, size_percent=65),
        widget.Chord(), # chord indicator
        widget.CurrentScreen(), # current screen indicator
        widget.Systray(),
        widget.Sep(padding=10, size_percent=65),
        widget.Backlight(backlight_name="intel_backlight", 
            fmt="brightness {}", format="{percent:2.0%}"),
        widget.Sep(padding=10, size_percent=65),
        widget.Clock(format="%Y-%m-%d %a %T")], # add clock
    24, # height
    background="#222222")

# create bar for secondary screen
# only difference between this and the other bar is
# the lack of all the extra widgets
secondary_bar = bar.Bar([ 
        widget.GroupBox(
            visible_groups=[f"{1}_{i}" for i in "123456789"],
            highlight_method="line",
            highlight_color=["222", "282828"],
            inactive="#aaa",
            hide_unused=True
        ),
        widget.CurrentLayout(),
        widget.WindowName(),
        widget.Spacer(length=bar.STRETCH),

        widget.Clock(format="%Y-%m-%d %a %T")
    ],
    24,
    background="#222222")   
