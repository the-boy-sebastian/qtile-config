import requests
import re
import subprocess
import shlex
from libqtile.log_utils import logger
from libqtile.widget import base


def get_np(user, key):
    # recent tracks api endpoint
    r = requests.get(
        f"http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user={user}&api_key={key}&format=json"
    )

    # convert result to json
    res = r.json()

    # check if song is playing
    try:
        for track in res["recenttracks"]["track"]:
            if "@attr" in track and track["@attr"]["nowplaying"] == "true":
                logger.info("Getting now playing...")
                artist = track["artist"]["#text"]
                title = track["name"]
                return f"{artist} - {title}"
        else:
            return "nothing playing"
    except KeyError:
        logger.info("No recent tracks")
        return "nothing playing"


class NowPlaying(base.ThreadPoolText):
    """Widget that grabs now playing from lastfm"""

    defaults = [
        ("username", None, "lastfm username to grab now playing from"),
        ("api_key", None, "lastfm api key"),
    ]

    def __init__(self, username, api_key, **config):
        base.ThreadPoolText.__init__(self, "", **config)
        self.add_defaults(NowPlaying.defaults)
        self.username = username
        self.api_key = api_key

    def poll(self):
        logger.info("LFM widget polled")
        if not self.username:
            return "You need to specify a username"
        elif not self.api_key:
            return "You need to specify an API key"
        try:
            return get_np(self.username, self.api_key)
        except Exception as e:
            return f"exc {type(e).__name__}{e.args}"


class UPowerBattRemaining(base.ThreadPoolText):
    def __init__(self, **config) -> None:
        base.ThreadPoolText.__init__(self, "", **config)
        self.ticker = 0

    def poll(self):
        self.ticker += 1
        upower_info = subprocess.run(shlex.split("upower -i /org/freedesktop/UPower/devices/battery_BAT0"), capture_output=True, encoding="utf-8")
        upower_stdout = list(map(lambda x: re.sub("\s\s+", " ", x), upower_info.stdout.split('\n')))
        upower_table = {}

        for i in upower_stdout:
            _i = i.split(":")
            if len(_i) == 2:
                upower_table.update({_i[0].strip(): _i[1].strip()})

        _status = ""
        _time = 0
        try:
            _time = float(''.join(filter(lambda x: x in "0123456789.", upower_table["time to empty"])))
            _status = "discharging"
        except KeyError:
            try:
                _time = float(''.join(filter(lambda x: x in "0123456789.", upower_table["time to full"])))
                _status = "charging"
            except KeyError: raise Exception("Nothing to show")

        if _status == "discharging":
            return f"{int(_time // 1)}h{round(_time % 1 * 60)}m left"
        elif _status == "charging":
            return f"{int(_time // 1)}m left"
