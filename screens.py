from libqtile.config import Screen
from bar import primary_bar, secondary_bar  # import bars
from copy import deepcopy

screens = [
    Screen(
        bottom=primary_bar,  # set bar
        wallpaper="/home/owittnan/.local/share/hackney-wick1.jpg",
        wallpaper_mode="fill"
    ),
    Screen(
        bottom=secondary_bar,  # set bar
        wallpaper="/home/owittnan/.local/share/hackney-wick1.jpg",
        wallpaper_mode="fill"
    ),
]
