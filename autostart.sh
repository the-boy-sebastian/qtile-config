#!/usr/bin/zsh

# way-displays > "/tmp/way-displays.${XDG_VTNR}.${USER}.log" 2>&1
picom -b
autorandr -c
xss-lock i3lock-fancy &
lxqt-policykit-agent &
setxkbmap -device $(xinput list --id-only "$(cat /proc/bus/input/devices | grep keyboard | cut -d'=' -f2 | tr -d '"')") -option caps:swapescape
dunst &
